using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControl : MonoBehaviour
{
    private int level;
    public int[,] map;
    public int[] destination; 
    public int[] start_pos;
    public GameObject[] map_prfab;
    
    void Awake()
    {
        LoadData();
        CreateMap();
    }

    // Update is called once per frame
    public void LoadData() {
        level = DataLevel.level;
        map = DataLevel.LoadMap(level);
        destination = DataLevel.LoadDesPos(level);
        start_pos = DataLevel.LoadStartPos(level);
    } 

    public void CreateMap() {
        GameObject mapp = (GameObject)Instantiate(map_prfab[level-1], new Vector3(0,0,0), Quaternion.identity);
        mapp.transform.parent = transform;
    }

    public Vector3 GetPosition(int row, int col) {
        return new Vector3(2*col, 0, 2*(map.GetLength(0)-1-row));
    }
}
