using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameButton : MonoBehaviour
{
    public void PlayAgain() {
        SceneManager.LoadScene(1);
    }

    public void NextLevel() {
        DataLevel.level += 1;
        SceneManager.LoadScene(1);
    }
}
