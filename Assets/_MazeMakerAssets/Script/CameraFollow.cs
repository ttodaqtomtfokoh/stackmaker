using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // public Transform target;
    // public Vector3 offset;

    // // Update is called once per frame
    // void Update()
    // {
    //     transform.position = target.position + offset;
    // }


    public Transform camPoint;
    public Transform player;
    public float heightFromGround;
    float yPos;

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(player.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            yPos = hit.point.y;
        }

        transform.position = new Vector3(camPoint.position.x, yPos + heightFromGround + camPoint.position.y, camPoint.position.z);
        transform.LookAt(player.position);
    }
}
