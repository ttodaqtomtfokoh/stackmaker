using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLevel
{
    public static int level = 1;
    public static int max_level = 5;

    // Start is called before the first frame update
    public static int[,] LoadMap(int level) {
        switch (level) {
            case 1:
                return new int[,] {{3,3,3,3,0,0,0},
                                   {3,0,0,1,0,0,0},
                                   {2,0,0,1,1,1,1}
                                  };
            case 2:
                return new int[,] {{3,1,1,1},
                                   {3,0,0,0},
                                   {3,1,1,3},
                                   {0,0,0,3},
                                   {3,3,3,3},
                                   {3,0,0,0},
                                   {2,0,0,0}
                                  };
            case 3:
                return new int[,] {{3,3,3,1,3,3,3},
                                   {3,0,0,3,0,0,1},
                                   {3,0,0,3,0,0,1}
                                  };
            case 4:
                return new int[,] {{0,0,1,0,0},
                                   {0,0,1,0,0},
                                   {3,3,1,1,1},
                                   {3,0,3,0,3},
                                   {3,0,3,3,3},
                                   {3,0,0,0,0}
                                  };
            case 5:
                return new int[,] {{3,3,3,0,1},
                                   {3,0,1,0,1},
                                   {3,3,1,3,3}
                                  };
        }
        return null;
    }

    public static int[] LoadStartPos(int level) {
        switch(level) {
            case 1:
                return new int[] {2, 0};
            case 2:
                return new int[] {6,0};
            case 3:
                return new int[] {2,3};
            case 4:
                return new int[] {5,0};
            case 5:
                return new int[] {2,0};
        }
        return null;
    }

    public static int[] LoadDesPos(int level) {
        switch (level) {
            case 1:
                return new int[] {2, 6};
            case 2:
                return new int[] {0,3};
            case 3:
                return new int[] {2,6};
            case 4:
                return new int[] {0,2};
            case 5:
                return new int[] {0,4};
        }
        return null;
    }
}
