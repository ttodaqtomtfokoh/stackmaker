using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public Vector3 Rotation;
    public bool Moving;
    public int stack;

    public bool finishlevel;
    public WinAnim winanim;

    public GameObject stackContainer;

    public List<GameObject> stack_list = new List<GameObject>();
    
    void Start()
    {
        transform.eulerAngles = Rotation;
    }

    // Update is called once per frame
    public void Win() {
        finishlevel = true;
        winanim.scoreText.text = "Score: " + stack.ToString();
        winanim.Anim();
    }

    public float GetHigh(int stack) {
        return stack*0.1f;
    }
}
