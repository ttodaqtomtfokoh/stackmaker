using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveControl : MonoBehaviour
{
    public enum Direction {Up, Down, Right, Left};
    public Direction direction;
    private float DeltaX;
    private float DeltaY;
    private float playerSpeed = 0.3f;
    public PlayerControl playerControl;
    public Transform Map;
    public int posX;
    public int posY;
    private int new_posX;
    private int new_posY;
    private Vector3 startvect;
    private Rigidbody rigitbody;
    private MapControl mapcontrol;

    void Start()
    {
        var childcount = Map.GetChild(0).childCount;
        for (int i = 0; i < childcount; ++i) {
            if (Map.GetChild(0).GetChild(i).name == "Stack") {
                playerControl.stack_list.Add(Map.GetChild(0).GetChild(i).gameObject);
            }
            else break;
        }
        playerControl.stack_list[0].transform.parent = playerControl.stackContainer.transform;
        var stackposs = new Vector3(0,0.05f,0);
        playerControl.stack_list[0].transform.position = new Vector3(0,0.05f,0);
        playerControl.stack_list[0].GetComponent<StackControl>().on_map = false;

        mapcontrol = Map.GetComponent<MapControl>();
        posX = mapcontrol.start_pos[1];
        posY = mapcontrol.start_pos[0];
        new_posX = posX;
        new_posY = posY;
        startvect = mapcontrol.GetPosition(posY, posX);
        startvect.y = playerControl.GetHigh(1);
        transform.position = startvect;
        rigitbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
            DeltaX = Input.GetAxis("Mouse X");
            DeltaY = Input.GetAxis("Mouse Y");
            if (Input.GetMouseButton(0) && !playerControl.finishlevel){
                if (Mathf.Abs(DeltaX) > Mathf.Abs(DeltaY)) {
                    if (DeltaX < 0) {
                        Move(Direction.Left);
                    }
                    else if (DeltaX > 0) {
                        Move(Direction.Right);
                    }
                }
                else if (Mathf.Abs(DeltaX) < Mathf.Abs(DeltaY)) {
                    if (DeltaY < 0) {
                        Move(Direction.Down);
                    }
                    else if (DeltaY > 0) {
                        Move(Direction.Up);
                    }
                }
            }
    }

    private void LockMouse() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void UnlockMouse() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }


    private void Move(Direction direction) {
        if (!playerControl.Moving) {
            // LockMouse();
            // playerControl.Moving = true;
            switch (direction)
            {
                case Direction.Left:
                    switch (playerControl.Rotation.y) {
                        case 180:
                            if (posX >= mapcontrol.map.GetLength(1)-1) break;
                            else {
                                for (int i=posX;i < mapcontrol.map.GetLength(1);++i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y-90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y -= 90;
                                Go_90();
                            }
                            break;
                        case 0:
                            if (posX <= 0) break;
                            else {
                                for (int i = posX; i >= 0; --i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y-90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y -= 90;
                                Go_270();
                            }
                            break;
                        case 90:
                            if (posY <= 0) break;
                            else {
                                for (int i = posY; i >= 0; --i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y-90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y -= 90;
                                Go_0();
                            }
                            break;
                        case 270:
                            if (posY >= mapcontrol.map.GetLength(0)-1) break;
                            else {
                                for (int i = posY; i < mapcontrol.map.GetLength(0); ++i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y-90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y -= 90;
                                Go_180();
                            }
                            break;
                    }
                    break;
                case Direction.Right:
                    switch (playerControl.Rotation.y) {
                        case 0:
                            if (posX >= mapcontrol.map.GetLength(1)-1) break;
                            else {
                                for (int i=posX;i < mapcontrol.map.GetLength(1);++i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 90;
                                Go_90();
                            }
                            break;
                        case 180:
                            if (posX <= 0) break;
                            else {
                                for (int i = posX; i >= 0; --i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 90;
                                Go_270();
                            }
                            break;
                        case 90:
                            if (posY >= mapcontrol.map.GetLength(0)-1) break;
                            else {
                                for (int i = posY; i < mapcontrol.map.GetLength(0); ++i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 90;
                                Go_180();
                            }
                            break;
                        case 270:
                            if (posY <= 0) break;
                            else {
                                for (int i = posY; i >= 0; --i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+90,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 90;
                                Go_0();
                            }
                            break;
                    }
                    break;
                case Direction.Down:
                    switch(playerControl.Rotation.y) {
                        case 0:
                            if (posY >= mapcontrol.map.GetLength(0)-1) break;
                            else {
                                for (int i = posY; i < mapcontrol.map.GetLength(0); ++i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+180,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 180;
                                Go_180();
                            }
                            break;
                        case 180:
                            if (posY <= 0) break;
                            else {
                                for (int i = posY; i >= 0; --i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+180,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 180;
                                Go_0();
                            }
                            break;
                        case 90: 
                            if (posX <= 0) break;
                            else {
                                for (int i = posX; i >= 0; --i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+180,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 180;
                                Go_270();
                            }
                            break;
                        case 270:
                            if (posX >= mapcontrol.map.GetLength(1)-1) break;
                            else {
                                for (int i=posX;i < mapcontrol.map.GetLength(1);++i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                var rot = playerControl.Rotation;
                                rigitbody.DORotate(new Vector3(rot.x,rot.y+180,rot.z), 0.2f, RotateMode.Fast);
                                playerControl.Rotation.y += 180;
                                Go_90();
                            }
                            break;
                    }
                    break;
                case Direction.Up:
                    switch (playerControl.Rotation.y) {
                        case 0:
                            if (posY <= 0) break;
                            else {
                                for (int i = posY; i >= 0; --i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i;
                                    }
                                    else break;
                                }
                                Go_0();
                                
                            }
                            break;
                        case 90:
                            if (posX >= mapcontrol.map.GetLength(1)-1) break;
                            else {
                                for (int i=posX;i < mapcontrol.map.GetLength(1);++i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                Go_90();
                            }
                            break;
                        case 180:
                            if (posY >= mapcontrol.map.GetLength(0)-1) break;
                            else {
                                for (int i = posY; i < mapcontrol.map.GetLength(0); ++i) {
                                    if (mapcontrol.map[i, posX] >= 1) {
                                        new_posY = i; 
                                    }
                                    else break;
                                }
                                Go_180();
                            }
                            break;
                        case 270:
                            if (posX <= 0) break;
                            else {
                                for (int i = posX; i >= 0; --i) {
                                    if (mapcontrol.map[posY,i] >= 1) {
                                        new_posX = i;
                                    }
                                    else break;
                                }
                                Go_270();
                            }
                            break;
                        } 
                    break;
            }
        }
    }

    private void Go_0() {
        if (new_posY != posY) {
            StartMove();
            posY -= 1;
            if (mapcontrol.map[posY, posX] == 3) {
                playerControl.stack += 1;
            }
            else if (mapcontrol.map[posY, posX] == 1) {
                playerControl.stack -= 1;
            }
            var new_pos = mapcontrol.GetPosition(posY, posX);
            new_pos.y = playerControl.GetHigh(playerControl.stack);
            transform.DOMove(new_pos, playerSpeed)
                .OnComplete(() => {
                     //repeat
                    CheckStack(posX, posY);
                    Go_0();
                });;
        }
        else {
            StopMove();
            return;
        }
    }
    private void Go_180() {
        if (new_posY != posY) {
            StartMove();
            posY += 1;
            if (mapcontrol.map[posY, posX] == 3) {
                playerControl.stack += 1;
            }
            else if (mapcontrol.map[posY, posX] == 1) {
                playerControl.stack -= 1;
            }
            var new_pos = mapcontrol.GetPosition(posY, posX);
            new_pos.y = playerControl.GetHigh(playerControl.stack);
            transform.DOMove(new_pos, playerSpeed)
                .OnComplete(() => {
                     //repeat
                    CheckStack(posX, posY);
                    Go_180();
                });;
        }
        else {
            StopMove();
            return;
        }
    }

    private void Go_90() {
        if (new_posX != posX) {
            StartMove();
            posX += 1;
            if (mapcontrol.map[posY, posX] == 3) {
                playerControl.stack += 1;
            }
            else if (mapcontrol.map[posY, posX] == 1) {
                playerControl.stack -= 1;
            }
            var new_pos = mapcontrol.GetPosition(posY, posX);
            new_pos.y = playerControl.GetHigh(playerControl.stack);
            transform.DOMove(new_pos, playerSpeed)
                .OnComplete(() => {
                     //repeat
                    CheckStack(posX, posY);
                    Go_90();
                });;
        }
        else {
            StopMove();
            return;
        }
    }

    private void Go_270() {
        if (new_posX != posX) {
            StartMove();
            posX -= 1;
            if (mapcontrol.map[posY, posX] == 3) {
                playerControl.stack += 1;
            }
            else if (mapcontrol.map[posY, posX] == 1) {
                playerControl.stack -= 1;
            }
            var new_pos = mapcontrol.GetPosition(posY, posX);
            new_pos.y = playerControl.GetHigh(playerControl.stack);
            transform.DOMove(new_pos, playerSpeed)
                .OnComplete(() => {
                     //repeat
                    CheckStack(posX, posY);
                    Go_270();
                });;
        }
        else {
            StopMove();
            return;
        }
    }

    private void CheckStack(int posX, int posY) {
        if (mapcontrol.map[posY, posX] == 3) {
            mapcontrol.map[posY, posX] = 2;
            for (int i = 0; i < playerControl.stack_list.Count; ++i) {
                if (playerControl.stack_list[i].GetComponent<StackControl>().on_map) {
                    var tmp = playerControl.stack_list[i].transform.position;
                    if (tmp.x == mapcontrol.GetPosition(posY, posX).x && tmp.z == mapcontrol.GetPosition(posY, posX).z) {
                        playerControl.stack_list[i].transform.parent = playerControl.stackContainer.transform;
                        playerControl.stack_list[i].GetComponent<StackControl>().on_map = false;
                        break;
                    }
                }
            }
        }
        else if (mapcontrol.map[posY, posX] == 1) {
            mapcontrol.map[posY, posX] = 2;
            var min_y = 0.0f;
            var min_stack = -1;
            for (int i = 0; i < playerControl.stack_list.Count; ++i) {
                if (!playerControl.stack_list[i].GetComponent<StackControl>().on_map) {
                    if (playerControl.stack_list[i].GetComponent<StackControl>().Get_Y() < min_y) {
                        min_y = playerControl.stack_list[i].GetComponent<StackControl>().Get_Y();
                        min_stack = i;
                    }
                }
            }
            playerControl.stack_list[min_stack].transform.parent = Map.GetChild(0);
            var tmp = playerControl.stack_list[min_stack].transform.position;
            tmp.y = 0.05f;
            playerControl.stack_list[min_stack].transform.position = tmp;
            playerControl.stack_list[min_stack].GetComponent<StackControl>().on_map = true;
        }
    }

        private void StartMove() {
            LockMouse();
            playerControl.Moving = true;
        }

        private void StopMove() {
            posX = new_posX;
            posY = new_posY;
            playerControl.Moving = false;
            UnlockMouse();
            if (playerControl.Rotation.y < 0) {
                playerControl.Rotation.y += 360;
            }
            else if (playerControl.Rotation.y >= 360) {
                playerControl.Rotation.y -= 360;
            }
            if (CheckWin()) {
                playerControl.Win();
            }
        }

        private bool CheckWin() {
            if (posX == mapcontrol.destination[1] && posY == mapcontrol.destination[0] && playerControl.stack > 0) 
                return true;
            else return false;
        }
}
