using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinAnim: MonoBehaviour
{
    public GameObject PlayAgain;
    public GameObject NextLevel;
    public GameObject Score;

    public Text scoreText;

    public void Anim() {
        if (DataLevel.level < DataLevel.max_level) {
            PlayAgain.SetActive(true);
            NextLevel.SetActive(true);
            Score.SetActive(true);
        }
        else {
            PlayAgain.SetActive(true);
            PlayAgain.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,-400);
            Score.SetActive(true);
        }
    }
}
