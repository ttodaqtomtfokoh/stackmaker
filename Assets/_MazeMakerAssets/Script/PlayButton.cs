using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    public GameObject Play;
    public GameObject Bar;
    public Image Clip;
    private float fillProgress = 0f;

    public void LoadGame() {
        Play.SetActive(false);
        Bar.SetActive(true);
        StartCoroutine(LoadingScene(1));
    }

    IEnumerator LoadingScene(int SceneIndex)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneIndex);
        while (!asyncLoad.isDone)
        {
            fillProgress = Mathf.Clamp01(asyncLoad.progress / 0.9f);
            Clip.fillAmount = fillProgress;
            yield return null;
        }
    }

    public void NextLevel() {
        DataLevel.level += 1;
        SceneManager.LoadScene(1);
    }
}
